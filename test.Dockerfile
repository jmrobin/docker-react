# test.Dockerfile
# docker image build -f test.Dockerfile -t jmrhub/docker-react .
# docker container run jmrhub/docker-react npm run test -- --coverage
FROM node:10-alpine

WORKDIR /app/

COPY package.json .

RUN npm install

COPY . .

CMD ["npm", "run", "test"]